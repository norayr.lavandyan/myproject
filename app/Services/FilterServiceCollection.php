<?php

namespace App\Services;

use App\Interfaces\FilterAnimalsInterface;

class FilterServiceCollection implements  FilterAnimalsInterface
{
    public function search($data,$people)
    {
        return $people->query()->where('age',$data->age)->get();
    }
}
