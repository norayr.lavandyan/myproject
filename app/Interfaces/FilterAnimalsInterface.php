<?php

namespace App\Interfaces;

interface FilterAnimalsInterface
{

    public function search($data,$animal);

}
