<?php

namespace App\Console\Commands;

use App\Models\Animal;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateAnimals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:animals {qanak?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Animals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $qanak = $this->argument('qanak');

        $i = $qanak != null ? $qanak : 1;

        $bar = $this->output->createProgressBar($i);
        $bar->start();
        for ($j = 1;$j<=$i;$j++){
            $name = Str::random(8);
            $age = rand(10,40);

            Animal::create([
                "name" => $name,
                "age" => $age
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->info("aaaaaaaaaaaa");
    }
}
