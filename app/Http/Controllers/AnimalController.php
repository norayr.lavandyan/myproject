<?php

namespace App\Http\Controllers;

use App\DTO\GetAnimal;
use App\Http\Requests\AnimalRequest;
use App\Interfaces\FilterAnimalsInterface;
use App\Models\Animal;


class AnimalController extends Controller
{
    public function search(AnimalRequest $request,Animal $animal,FilterAnimalsInterface $filter)
    {
        $data = new GetAnimal($request->validated());

        $filterData = $filter->search($data,$animal);

        return response()->json($filterData,200);
    }
}
