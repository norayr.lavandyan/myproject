<?php

namespace App\Providers;

use App\Interfaces\FilterAnimalsInterface;
use App\Services\FilterServiceCollection;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FilterAnimalsInterface::class,FilterServiceCollection::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
