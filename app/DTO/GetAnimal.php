<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class GetAnimal extends DataTransferObject
{
    public string $name;

    public string $age;

}
