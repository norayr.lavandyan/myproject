<?php

namespace Modules\Person\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Modules\Person\Entities\Person;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreatePersons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'person:create {qanak?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Persons';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $qanak = $this->argument('qanak');

        $i = $qanak != null ? $qanak : 1;

        $bar = $this->output->createProgressBar($i);
        $bar->start();
        for ($j = 1;$j<=$i;$j++){
            $name = Str::random(8);
            $age = rand(10,40);

            Person::create([
                "name" => $name,
                "age" => $age
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->info("aaaaaaaaaaaa");
    }


/**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
