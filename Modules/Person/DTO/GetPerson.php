<?php

namespace Modules\Person\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class GetPerson extends DataTransferObject
{
    public string $name;

    public string $age;

}
