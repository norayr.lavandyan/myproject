<?php

namespace Modules\Person\Interfaces;

interface FilterInterface {

    public function search($data,$person);

}
