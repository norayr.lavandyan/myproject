<?php

namespace Modules\Person\Services;

use Modules\Person\Interfaces\FilterInterface;

class FilterServiceCollection implements FilterInterface
{
    public function search($data,$people)
    {
        return $people->query()->where('age',$data->age)->get();
    }
}
