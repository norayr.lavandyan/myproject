<?php

namespace Modules\Person\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Person extends Model
{
    use HasFactory;

    protected $guarded;

    protected static function newFactory()
    {
        return \Modules\Person\Database\factories\PersonFactory::new();
    }
}
