<?php

namespace Modules\Person\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Person\DTO\GetPerson;
use Modules\Person\Entities\Person;
use Modules\Person\Http\Requests\PersonRequest;
use Modules\Person\Interfaces\FilterInterface;
use Modules\Person\Transformers\PersonResource;
use Modules\Person\Transformers\Resource;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('person::index');
    }

    public function list(PersonRequest $request,FilterInterface $filter, Person $person)
    {
        $data = new GetPerson($request->validated());

        $filterData = $filter->search($data,$person);

        return new PersonResource($filterData);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('person::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('person::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('person::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
