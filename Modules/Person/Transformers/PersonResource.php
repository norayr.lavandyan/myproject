<?php

namespace Modules\Person\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PersonResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "data"=>$this->collection
        ];
    }
}
