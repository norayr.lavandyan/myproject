<?php

use App\Http\Controllers\AnimalController;
use Illuminate\Support\Facades\Route;
use Modules\Person\Http\Controllers\PersonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/gag', [PersonController::class, 'list'] )->name('get_users');
